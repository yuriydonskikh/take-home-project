using AutoFixture.Xunit2;
using CarsalesLight.Models.Entities;
using CarsalesLight.Models.ViewModels;
using CarsalesLight.Tests.Helpers;
using FluentAssertions;
using Xunit;

namespace CarsalesLight.Tests
{
    public class MappingTests
    {
        [Theory, AutoData]
        public void CustomerRegistrationViewModelToAppUser(CustomerRegistrationViewModel vm)
        {
            var result = MockHelper.Mapper.Value.Map<AppUser>(vm);
            result.Should().BeOfType<AppUser>();
        }

        [Theory, AutoData]
        public void VehicleToVehicleViewModel(Vehicle vm)
        {
            var result = MockHelper.Mapper.Value.Map<VehicleViewModel>(vm);
            result.Should().BeOfType<VehicleViewModel>();
        }

        [Theory, AutoData]
        public void VehicleOptionToOptionViewModel(OptionViewModel vm, VehicleOption om)
        {
            var resultom = MockHelper.Mapper.Value.Map<VehicleOption>(vm);
            resultom.Should().BeOfType<VehicleOption>();
            var resultvm = MockHelper.Mapper.Value.Map<OptionViewModel>(om);
            resultvm.Should().BeOfType<OptionViewModel>();
        }

        [Theory, AutoData]
        public void VehicleToVehicleQueryViewModel(Vehicle vm)
        {
            var result = MockHelper.Mapper.Value.Map<VehicleQueryViewModel>(vm);
            result.Should().BeOfType<VehicleQueryViewModel>();
        }

        [Theory, AutoData]
        public void VehicleTypeToInfoViewModel(InfoViewModel vm, VehicleType om)
        {
            var resultom = MockHelper.Mapper.Value.Map<VehicleType>(vm);
            resultom.Should().BeOfType<VehicleType>();
            var resultvm = MockHelper.Mapper.Value.Map<InfoViewModel>(om);
            resultvm.Should().BeOfType<InfoViewModel>();
        }

        [Theory, AutoData]
        public void VehicleMakeToInfoViewModel(InfoViewModel vm, VehicleMake om)
        {
            var resultom = MockHelper.Mapper.Value.Map<VehicleMake>(vm);
            resultom.Should().BeOfType<VehicleMake>();
            var resultvm = MockHelper.Mapper.Value.Map<InfoViewModel>(om);
            resultvm.Should().BeOfType<InfoViewModel>();
        }

        [Theory, AutoData]
        public void VehicleModelToMakeModelViewModel(VehicleModel vm)
        {
            var result = MockHelper.Mapper.Value.Map<MakeModelViewModel>(vm);
            result.Should().BeOfType<MakeModelViewModel>();
        }

        [Theory, AutoData]
        public void MakeModelViewModelToVehicleModel(MakeModelViewModel vm)
        {
            var result = MockHelper.Mapper.Value.Map<VehicleModel>(vm);
            result.Should().BeOfType<VehicleModel>();
        }
    }
}
