using System.Collections.Generic;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using CarsalesLight.Controllers;
using CarsalesLight.Models.Entities;
using CarsalesLight.Models.ViewModels;
using CarsalesLight.Tests.Helpers;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace CarsalesLight.Tests
{
    public class VehicleControllerTests
    {
        private readonly DataBaseContext _context;

        public VehicleControllerTests()
        {
            _context = new DataBaseContext();
        }

        private async Task<VehicleController> GetController()
        {
            await _context.GetContext();
            return new VehicleController(_context.Context, MockHelper.MockHttpContextAccessor(_context.Context), MockHelper.MockLogger<VehicleController>());
        }

        [Fact]
        public async Task Get()
        {
            try
            {
                var controller = await GetController();

                var result = await controller.Get();
                result.Should().BeOfType<OkObjectResult>()
                    .Which.Value.Should().BeOfType<List<VehicleViewModel>>()
                    .Which.Count.Should().Be(1);
            }
            finally
            {
                _context.Close();
            }
        }

        [Theory, AutoData]
        public async Task Post(VehicleViewModel model)
        {
            try
            {
                var controller = await GetController();

                var result = await controller.Post(model);
                result.Should().BeOfType<OkObjectResult>()
                    .Which.Value.Should().BeOfType<Vehicle>()
                    .Which.Model.Model.Should().Be(model.MakeModel.Model);
            }
            finally
            {
                _context.Close();
            }
        }

        [Theory, AutoData]
        public async Task Put(VehicleViewModel model)
        {
            try
            {
                var controller = await GetController();

                var result = await controller.Put(1, model);
                result.Should().BeOfType<OkObjectResult>()
                    .Which.Value.Should().BeOfType<Vehicle>()
                    .Which.Model.Model.Should().Be(model.MakeModel.Model);
            }
            finally
            {
                _context.Close();
            }
        }

        [Fact]
        public async Task Delete()
        {
            try
            {
                var controller = await GetController();

                var result = await controller.Delete(1);
                result.Should().BeOfType<OkResult>();
            }
            finally
            {
                _context.Close();
            }
        }
    }
}
