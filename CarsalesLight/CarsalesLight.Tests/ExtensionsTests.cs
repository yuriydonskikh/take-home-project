using System;
using System.Linq;
using AutoFixture.Xunit2;
using CarsalesLight.Helpers;
using CarsalesLight.Models.Entities;
using CarsalesLight.Models.ViewModels;
using CarsalesLight.Tests.Helpers;
using FluentAssertions;
using Xunit;

namespace CarsalesLight.Tests
{
    public class ExtensionsTests
    {
        [Fact]
        public void BetweenOrDefault()
        {
            var intProbe = 100;
            var result = intProbe.BetweenOrDefault(99,101,0);
            result.Should().BeTrue();
            result = intProbe.BetweenOrDefault(100, 100, 0);
            result.Should().BeTrue();
            result = intProbe.BetweenOrDefault(0, 0, 0);
            result.Should().BeTrue();
            result = intProbe.BetweenOrDefault(0, 101, 0);
            result.Should().BeTrue();
            result = intProbe.BetweenOrDefault(99, 0, 0);
            result.Should().BeTrue();
            result = intProbe.BetweenOrDefault(98, 99, 0);
            result.Should().BeFalse();
            result = intProbe.BetweenOrDefault(101, 102, 0);
            result.Should().BeFalse();
            result = intProbe.BetweenOrDefault(101, 99, 0);
            result.Should().BeFalse();
        }

        [Theory, AutoData]
        public void ContainsText(VehicleViewModel model)
        {
            var result = model.ContainsText(model.Options.First().Comment.Substring(7, 5));
            result.Should().BeTrue();
        }

        [Theory, AutoData]
        public void IsQueryable(Vehicle model)
        {
            var result = model.IsQueryable(MockHelper.Mapper.Value.Map<VehicleQueryViewModel>(model));
            result.Should().BeTrue();
        }

        [Fact]
        public void ToUnixDate()
        {
            var result = new DateTime(1970, 1, 1).AddSeconds(100).ToUnixDate();
            result.Should().Be(100);
        }
    }
}
