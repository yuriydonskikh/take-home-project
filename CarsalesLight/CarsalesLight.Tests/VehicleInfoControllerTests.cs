using System.Threading.Tasks;
using AutoFixture.Xunit2;
using CarsalesLight.Controllers;
using CarsalesLight.Models.Entities;
using CarsalesLight.Models.ViewModels;
using CarsalesLight.Tests.Helpers;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace CarsalesLight.Tests
{
    public class VehicleInfoControllerTests
    {
        private readonly DataBaseContext _context;

        public VehicleInfoControllerTests()
        {
            _context = new DataBaseContext();
        }

        private async Task<VehicleInfoController> GetController()
        {
            await _context.GetContext();
            return new VehicleInfoController(_context.Context, MockHelper.MockLogger<VehicleInfoController>());
        }

        [Theory, AutoData]
        public async Task PutVehicleType(InfoViewModel mockType)
        {
            try
            {
                var controller = await GetController();

                var result = await controller.PutVehicleType(mockType);
                result.Should().BeOfType<OkObjectResult>().Which
                    .Value.Should().BeOfType<VehicleType>().Which
                    .Type.Should().Be(mockType.Info);
            }
            finally 
            {
                _context.Close();
            }
        }

        [Theory, AutoData]
        public async Task PutVehicleOption(OptionViewModel mockModel)
        {
            try
            {
                var controller = await GetController();

                var result = await controller.PutVehicleOption(1, mockModel);
                result.Should().BeOfType<OkObjectResult>().Which
                    .Value.Should().BeOfType<VehicleOption>().Which
                    .Option.Should().Be(mockModel.Option);
            }
            finally
            {
                _context.Close();
            }
        }

        [Fact]
        public async Task DeleteVehicleOption()
        {
            try
            {
                var controller = await GetController();

                var result = await controller.DeleteVehicleOption(1);
                result.Should().BeOfType<OkResult>();
            }
            finally
            {
                _context.Close();
            }
        }

        [Fact]
        public async Task DeleteVehicleOptionNegotive()
        {
            try
            {
                var controller = await GetController();

                var result = await controller.DeleteVehicleOption(0);
                result.Should().BeOfType<BadRequestObjectResult>();
            }
            finally
            {
                _context.Close();
            }
        }

        [Theory, AutoData]
        public async Task AddVehicleMakeModel(MakeModelViewModel mockModel)
        {
            try
            {
                var controller = await GetController();

                var result = await controller.AddVehicleMakeModel(mockModel);
                result.Should().BeOfType<OkObjectResult>().Which
                    .Value.Should().BeOfType<VehicleModel>().Which
                    .Model.Should().Be(mockModel.Model);
            }
            finally
            {
                _context.Close();
            }
        }

        [Fact]
        public async Task DeleteVehicleMake()
        {
            try
            {
                var controller = await GetController();

                var result = await controller.DeleteVehicleMake(1);
                result.Should().BeOfType<OkResult>();
            }
            finally
            {
                _context.Close();
            }
        }

        [Fact]
        public async Task DeleteVehicleModel()
        {
            try
            {
                var controller = await GetController();

                var result = await controller.DeleteVehicleModel(1);
                result.Should().BeOfType<OkResult>();
            }
            finally
            {
                _context.Close();
            }
        }
    }
}
