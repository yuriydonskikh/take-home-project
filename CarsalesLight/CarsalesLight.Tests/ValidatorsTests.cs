﻿using System;
using CarsalesLight.Models.Validations;
using CarsalesLight.Models.ViewModels;
using FluentValidation.TestHelper;
using Xunit;

namespace CarsalesLight.Tests
{
    public class ValidatorsTests
    {
        [Fact]
        public void CredentialsViewModelValidator()
        {
            var validator = new CredentialsViewModelValidator();
            validator.ShouldHaveValidationErrorFor(e => e.UserName, string.Empty);
            validator.ShouldHaveValidationErrorFor(e => e.Password, string.Empty);
            validator.ShouldNotHaveValidationErrorFor(e => e.Password, "123456");
            validator.ShouldNotHaveValidationErrorFor(e => e.Password, "123456789");
            validator.ShouldNotHaveValidationErrorFor(e => e.Password, "123456789112");
            validator.ShouldHaveValidationErrorFor(e => e.Password, "1234567891123");
        }

        [Fact]
        public void MakeModelValidator()
        {
            var validator = new MakeModelValidator();
            validator.ShouldHaveValidationErrorFor(e => e.Make, string.Empty);
            validator.ShouldHaveValidationErrorFor(e => e.Model, string.Empty);
            validator.ShouldNotHaveValidationErrorFor(e => e.Make, "0");
            validator.ShouldNotHaveValidationErrorFor(e => e.Model, "0");
        }

        [Fact]
        public void VehicleOptionValidator()
        {
            var validator = new VehicleOptionValidator();
            validator.ShouldHaveValidationErrorFor(e => e.Option, string.Empty);
            validator.ShouldNotHaveValidationErrorFor(e => e.Option, "0");
        }

        [Fact]
        public void VehicleQueryValidator()
        {
            var validator = new VehicleQueryValidator();
            validator.ShouldNotHaveValidationErrorFor(e => e.Make, new VehicleQueryViewModel{Type = "1"});
            validator.ShouldNotHaveValidationErrorFor(e => e.Model, new VehicleQueryViewModel { Make = "1" });
            validator.ShouldNotHaveValidationErrorFor(e => e.Type, new VehicleQueryViewModel { Model = "1" });
            validator.ShouldHaveValidationErrorFor(e => e.Type, new VehicleQueryViewModel());

            validator.ShouldNotHaveValidationErrorFor(e => e.IncludeDealer, new VehicleQueryViewModel { IncludePrivate = true });
            validator.ShouldNotHaveValidationErrorFor(e => e.IncludePrivate, new VehicleQueryViewModel { IncludeDealer = true });
            validator.ShouldHaveValidationErrorFor(e => e.IncludeDealer, new VehicleQueryViewModel());

            validator.ShouldNotHaveValidationErrorFor(e=>e.YearFrom, 0);
            validator.ShouldNotHaveValidationErrorFor(e => e.YearFrom, DateTime.Now.Year);
            validator.ShouldNotHaveValidationErrorFor(e => e.YearFrom, DateTime.Now.AddYears(-150).Year);
            validator.ShouldNotHaveValidationErrorFor(e => e.YearFrom, DateTime.Now.AddYears(1).Year);
            validator.ShouldHaveValidationErrorFor(e => e.YearFrom, DateTime.Now.AddYears(-151).Year);
            validator.ShouldHaveValidationErrorFor(e => e.YearFrom, DateTime.Now.AddYears(2).Year);

            validator.ShouldNotHaveValidationErrorFor(e => e.YearTo, 0);
            validator.ShouldNotHaveValidationErrorFor(e => e.YearTo, DateTime.Now.Year);
            validator.ShouldNotHaveValidationErrorFor(e => e.YearTo, DateTime.Now.AddYears(-150).Year);
            validator.ShouldNotHaveValidationErrorFor(e => e.YearTo, DateTime.Now.AddYears(1).Year);
            validator.ShouldHaveValidationErrorFor(e => e.YearTo, DateTime.Now.AddYears(-151).Year);
            validator.ShouldHaveValidationErrorFor(e => e.YearTo, DateTime.Now.AddYears(2).Year);
        }

        [Fact]
        public void VehicleTypeValidator()
        {
            var validator = new VehicleTypeValidator();
            validator.ShouldHaveValidationErrorFor(e => e.Type, string.Empty);
            validator.ShouldNotHaveValidationErrorFor(e => e.Type, "0");
        }

        [Fact]
        public void VehicleValidator()
        {
            var validator = new VehicleValidator();
            validator.ShouldNotHaveValidationErrorFor(e => e.Type, "0");
            validator.ShouldHaveValidationErrorFor(e => e.Type, string.Empty);

            validator.ShouldNotHaveValidationErrorFor(e => e.Year, DateTime.Now.Year);
            validator.ShouldNotHaveValidationErrorFor(e => e.Year, DateTime.Now.AddYears(-150).Year);
            validator.ShouldNotHaveValidationErrorFor(e => e.Year, DateTime.Now.AddYears(1).Year);
            validator.ShouldHaveValidationErrorFor(e => e.Year, DateTime.Now.AddYears(-151).Year);
            validator.ShouldHaveValidationErrorFor(e => e.Year, DateTime.Now.AddYears(2).Year);

            validator.ShouldNotHaveValidationErrorFor(e => e.Price, 0.01M);
            validator.ShouldHaveValidationErrorFor(e => e.Price, -0.01M);
            validator.ShouldNotHaveValidationErrorFor(e => e.Price, 0M);
        }
    }
}
