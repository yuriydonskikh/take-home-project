using System.Collections.Generic;
using System.Threading.Tasks;
using CarsalesLight.Controllers;
using CarsalesLight.Models.ViewModels;
using CarsalesLight.Tests.Helpers;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace CarsalesLight.Tests
{
    public class RetrieveVehiclesControllerTests
    {
        private readonly DataBaseContext _context;

        public RetrieveVehiclesControllerTests()
        {
            _context = new DataBaseContext();
        }

        private async Task<RetrieveVehiclesController> GetController()
        {
            await _context.GetContext();
            return new RetrieveVehiclesController(_context.Context, MockHelper.Mapper.Value, MockHelper.MockLogger<RetrieveVehiclesController>());
        }

        [Fact]
        public async Task GetVehicles()
        {
            try
            {
                var controller = await GetController();

                var result = await controller.GetVehicles(MockHelper.MockVehicleQuery(_context.Context));
                result.Should().BeOfType<OkObjectResult>()
                    .Which.Value.Should().BeOfType<List<VehicleViewModel>>()
                    .Which.Count.Should().BeGreaterThan(0);
            }
            finally
            {
                _context.Close();
            }
        }

        [Fact]
        public async Task GetVehicle()
        {
            try
            {
                var controller = await GetController();

                var result = await controller.GetVehicle(1);
                result.Should().BeOfType<OkObjectResult>()
                    .Which.Value.Should().BeOfType<VehicleViewModel>();
            }
            finally
            {
                _context.Close();
            }
        }

        [Fact]
        public async Task GetMakes()
        {
            try
            {
                var controller = await GetController();

                var result = await controller.GetMakes();
                result.Should().BeOfType<OkObjectResult>()
                    .Which.Value.Should().BeOfType<List<InfoViewModel>>()
                    .Which.Count.Should().BeGreaterThan(0);
            }
            finally
            {
                _context.Close();
            }
        }

        [Fact]
        public async Task GetModels()
        {
            try
            {
                var controller = await GetController();

                var result = await controller.GetModels();
                result.Should().BeOfType<OkObjectResult>()
                    .Which.Value.Should().BeOfType<List<MakeModelViewModel>>()
                    .Which.Count.Should().BeGreaterThan(0);
            }
            finally
            {
                _context.Close();
            }
        }

        [Fact]
        public async Task GetTypes()
        {
            try
            {
                var controller = await GetController();

                var result = await controller.GetTypes();
                result.Should().BeOfType<OkObjectResult>()
                    .Which.Value.Should().BeOfType<List<InfoViewModel>>()
                    .Which.Count.Should().BeGreaterThan(0);
            }
            finally
            {
                _context.Close();
            }
        }
    }
}
