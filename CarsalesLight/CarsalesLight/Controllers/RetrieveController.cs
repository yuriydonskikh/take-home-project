﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using CarsalesLight.Data;
using CarsalesLight.Helpers;
using CarsalesLight.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CarsalesLight.Controllers
{
    [Produces("application/json"), Route("api/[controller]/[action]")]
    public class RetrieveVehiclesController:Controller
    {
        private readonly ApplicationDbContext _applicationDbContext;
        private readonly IMapper _mapper;
        private readonly ILogger<RetrieveVehiclesController> _logger;

        public RetrieveVehiclesController(ApplicationDbContext applicationDbContext, IMapper mapper, ILogger<RetrieveVehiclesController> logger)
        {
            _applicationDbContext = applicationDbContext;
            _mapper = mapper;
            _logger = logger;
        }

        /// <summary>
        /// Get vehicles use query
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetVehicles([FromQuery]VehicleQueryViewModel query)
        {
            _logger.LogInformation("GetVehicles");
            try
            {
                if (!ModelState.IsValid) return BadRequest(ModelState);
                var result = await _applicationDbContext.GetVehiclesAsync(query);
                return new OkObjectResult(result);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetVehicles");
                ModelState.AddErrorToModelState(e);
                return BadRequest(ModelState);
            }
        }

        /// <summary>
        /// Get vehicle by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetVehicle(int id)
        {
            _logger.LogInformation("GetVehicle");
            try
            {
                if (!ModelState.IsValid) return BadRequest(ModelState);
                var result = await _applicationDbContext.GetVehicleAsync(id);
                return new OkObjectResult(_mapper.Map<VehicleViewModel>(result));
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetVehicle");
                ModelState.AddErrorToModelState(e);
                return BadRequest(ModelState);
            }
        }

        /// <summary>
        /// Get vehicle makes
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetMakes()
        {
            _logger.LogInformation("GetMakes");
            try
            {
                if (!ModelState.IsValid) return BadRequest(ModelState);
                var result = await _applicationDbContext.GetMakes();
                return new OkObjectResult(_mapper.Map<List<InfoViewModel>>(result));
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetMakes");
                ModelState.AddErrorToModelState(e);
                return BadRequest(ModelState);
            }
        }

        /// <summary>
        /// Get vehicle models
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetModels()
        {
            _logger.LogInformation("GetModels");
            try
            {
                if (!ModelState.IsValid) return BadRequest(ModelState);
                var result = await _applicationDbContext.GetModels();
                return new OkObjectResult(_mapper.Map<List<MakeModelViewModel>>(result));
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetModels");
                ModelState.AddErrorToModelState(e);
                return BadRequest(ModelState);
            }
        }

        /// <summary>
        /// Get vehicle types
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetTypes()
        {
            _logger.LogInformation("GetTypes");
            try
            {
                if (!ModelState.IsValid) return BadRequest(ModelState);
                var result = await _applicationDbContext.GetTypes();
                return new OkObjectResult(_mapper.Map<List<InfoViewModel>>(result));
            }
            catch (Exception e)
            {
                _logger.LogError(e, "GetTypes");
                ModelState.AddErrorToModelState(e);
                return BadRequest(ModelState);
            }
        }

    }
}
