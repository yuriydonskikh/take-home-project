﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using CarsalesLight.Data;
using CarsalesLight.Helpers;
using CarsalesLight.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CarsalesLight.Controllers
{
    [Produces("application/json"), Authorize(Policy = "ApiUser"), Route("api/[controller]")]
    public class VehicleController:Controller
    {
        private readonly ApplicationDbContext _applicationDbContext;
        private readonly ILogger<VehicleController> _logger;
        private readonly ClaimsPrincipal _caller;

        public VehicleController(ApplicationDbContext applicationDbContext, IHttpContextAccessor httpContextAccessor, ILogger<VehicleController> logger)
        {
            _applicationDbContext = applicationDbContext;
            _logger = logger;
            _caller = httpContextAccessor.HttpContext.User;
        }

        /// <summary>
        /// Get all vehicles created by user
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            _logger.LogInformation("Get");
            try
            {
                var userId = _caller.Claims.Single(c => c.Type == "id");
                var result = await _applicationDbContext.GetVehiclesAsync(userId.Value);
                return new OkObjectResult(result);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Get");
                ModelState.AddErrorToModelState(e);
                return BadRequest(ModelState);
            }
        }

        /// <summary>
        /// Add vehicle
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]VehicleViewModel model)
        {
            _logger.LogInformation("Post");
            try
            {
                if (!ModelState.IsValid) return BadRequest(ModelState);

                var userId = _caller.Claims.Single(c => c.Type == "id");
                var result = await _applicationDbContext.AddVehicleAsync(model, userId.Value);
                result.Customer = null; //remove sensitive information
                return new OkObjectResult(result);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Post");
                ModelState.AddErrorToModelState(e);
                return BadRequest(ModelState);
            }
        }

        /// <summary>
        /// Edit vehicle
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> Put(int id, [FromBody]VehicleViewModel model)
        {
            _logger.LogInformation("Put");
            try
            {
                if (id == 0) ModelState.AddErrorToModelState(new ArgumentOutOfRangeException(nameof(id)));
                if (!ModelState.IsValid) return BadRequest(ModelState);

                var result = await _applicationDbContext.EditVehicleAsync(id, model);
                return new OkObjectResult(result);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Put");
                ModelState.AddErrorToModelState(e);
                return BadRequest(ModelState);
            }
        }

        /// <summary>
        /// Delete vehicle
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            _logger.LogInformation("Delete");
            try
            {
                if (id == 0) ModelState.AddErrorToModelState(new ArgumentOutOfRangeException(nameof(id)));
                if (!ModelState.IsValid) return BadRequest(ModelState);

                await _applicationDbContext.DeleteVehicleAsync(id);
                return Ok();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Delete");
                ModelState.AddErrorToModelState(e);
                return BadRequest(ModelState);
            }
        }
    }
}
