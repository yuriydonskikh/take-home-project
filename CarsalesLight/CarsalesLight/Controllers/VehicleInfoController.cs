﻿using System;
using System.Threading.Tasks;
using CarsalesLight.Data;
using CarsalesLight.Helpers;
using CarsalesLight.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CarsalesLight.Controllers
{
    [Produces("application/json"), Authorize(Policy = "ApiUser"), Route("api/[controller]/[action]")]
    public class VehicleInfoController:Controller
    {
        private readonly ApplicationDbContext _applicationDbContext;
        private readonly ILogger<VehicleInfoController> _logger;

        public VehicleInfoController(ApplicationDbContext applicationDbContext, ILogger<VehicleInfoController> logger)
        {
            _applicationDbContext = applicationDbContext;
            _logger = logger;
        }

        /// <summary>
        /// Add or update type of vehicle
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> PutVehicleType([FromBody]InfoViewModel type)
        {
            _logger.LogInformation("PutVehicleType");
            try
            {
                if (!ModelState.IsValid) return BadRequest(ModelState);
                var vtype = await _applicationDbContext.AddUpdateVehicleTypeAsync(type.Info);
                return new OkObjectResult(vtype);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "PutVehicleType");
                ModelState.AddErrorToModelState(e);
                return BadRequest(ModelState);
            }
        }

        /// <summary>
        /// Add or update vehicle's option in option set
        /// </summary>
        /// <param name="vehicleId"></param>
        /// <param name="option"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> PutVehicleOption(int vehicleId, [FromBody] OptionViewModel option)
        {
            _logger.LogInformation("PutVehicleOption");
            try
            {
                if (!ModelState.IsValid) return BadRequest(ModelState);
                var options = await _applicationDbContext.AddUpdateVehicleOptionAsync(option, vehicleId);
                return new OkObjectResult(options);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "PutVehicleOption");
                ModelState.AddErrorToModelState(e);
                return BadRequest(ModelState);
            }
        }

        /// <summary>
        /// Delete option
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteVehicleOption(int id)
        {
            _logger.LogInformation("DeleteVehicleOption");
            try
            {
                await _applicationDbContext.DeleteOptionAsync(id);
                return Ok();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "DeleteVehicleOption");
                ModelState.AddErrorToModelState(e);
                return BadRequest(ModelState);
            }
        }

        /// <summary>
        /// Add vehicle's make and model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> AddVehicleMakeModel([FromBody] MakeModelViewModel model)
        {
            _logger.LogInformation("AddVehicleMakeModel");
            try
            {
                if (!ModelState.IsValid) return BadRequest(ModelState);
                var result = await _applicationDbContext.AddMakeModelAsync(model);
                return new OkObjectResult(result);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "AddVehicleMakeModel");
                ModelState.AddErrorToModelState(e);
                return BadRequest(ModelState);
            }
        }

        /// <summary>
        /// Delete Make
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteVehicleMake(int id)
        {
            _logger.LogInformation("DeleteVehicleMake");
            try
            {
                await _applicationDbContext.DeleteMakeAsync(id);
                return Ok();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "DeleteVehicleMake");
                ModelState.AddErrorToModelState(e);
                return BadRequest(ModelState);
            }
        }

        /// <summary>
        /// Delete Model
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteVehicleModel(int id)
        {
            _logger.LogInformation("DeleteVehicleModel");
            try
            {
                await _applicationDbContext.DeleteModelAsync(id);
                return Ok();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "DeleteVehicleModel");
                ModelState.AddErrorToModelState(e);
                return BadRequest(ModelState);
            }
        }
    }
}
