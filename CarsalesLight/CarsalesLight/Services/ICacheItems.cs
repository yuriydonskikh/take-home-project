﻿namespace CarsalesLight.Services
{
    public interface ICacheItem
    {
        T Get<T>();
    }
}