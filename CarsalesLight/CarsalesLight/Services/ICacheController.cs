﻿using System;

namespace CarsalesLight.Services
{
    public interface ICacheController
    {
        TItem Use<TItem>(string key, Func<TItem> builder);
        void Invalidate(string key);
    }
}