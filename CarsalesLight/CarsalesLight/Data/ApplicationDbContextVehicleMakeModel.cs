﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CarsalesLight.Models.Entities;
using CarsalesLight.Models.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace CarsalesLight.Data
{
    public partial class ApplicationDbContext
    {
        private const string MAKE_CACHE_KEY = "make-cache-key";
        private const string MODEL_CACHE_KEY = "model-cache-key";

        public async Task<VehicleModel> AddMakeModelAsync(MakeModelViewModel makeModel)
        {
            var make = await GetCreateMakeAsync(makeModel.Make);
            var result = await VehicleModels.Include(i => i.Make).FirstOrDefaultAsync(i => i.Model == makeModel.Model);
            if (result != null) return result;

            result = new VehicleModel
            {
                Model = makeModel.Model,
                MakeId = make.Id
            };
            VehicleModels.Add(result);
            await SaveChangesAsync();
            _cache.Invalidate(MODEL_CACHE_KEY);
            return result;
        }

        public async Task<VehicleMake> GetCreateMakeAsync(string make)
        {
            var result = await VehicleMakes.FirstOrDefaultAsync(i => i.Make == make);
            if (result == null)
            {
                result = new VehicleMake{Make = make};
                VehicleMakes.Add(result);
                await SaveChangesAsync();
                _cache.Invalidate(MAKE_CACHE_KEY);
            }

            return result;
        }

        public async Task DeleteMakeAsync(int id)
        {
            var item = await VehicleMakes.FirstOrDefaultAsync(i => i.Id == id);
            VehicleMakes.Remove(item);
            await SaveChangesAsync();
            _cache.Invalidate(MAKE_CACHE_KEY);
        }

        public async Task DeleteModelAsync(int id)
        {
            var item = await VehicleModels.FirstOrDefaultAsync(i => i.Id == id);
            VehicleModels.Remove(item);
            await SaveChangesAsync();
            _cache.Invalidate(MODEL_CACHE_KEY);
        }

        public async Task<List<VehicleMake>> GetMakes()
        {
            return await Task.Run(() => { return _cache.Use(MAKE_CACHE_KEY, () => VehicleMakes.ToList()); });
        }

        public async Task<List<VehicleModel>> GetModels()
        {
            return await Task.Run(() => { return _cache.Use(MODEL_CACHE_KEY, () => VehicleModels.Include(i=>i.Make).ToList()); });
        }
    }
}
