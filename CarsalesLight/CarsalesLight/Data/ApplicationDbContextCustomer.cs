﻿using System.Threading.Tasks;
using CarsalesLight.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace CarsalesLight.Data
{
    public partial class ApplicationDbContext
    {
        public async Task AddCustomerAsync(string identityId)
        {
            await Customers.AddAsync(new Customer {IdentityId = identityId});
            await SaveChangesAsync();
        }

        public async Task<Customer> GetCustomerAsync(string userId)
        {
            return await Customers.Include(c => c.Identity).FirstOrDefaultAsync(c => c.Identity.Id == userId);
        }
    }
}
