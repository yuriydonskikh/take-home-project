﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CarsalesLight.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace CarsalesLight.Data
{
    public partial class ApplicationDbContext
    {
        private const string TYPES_CACHE_KEY = "types-cache-key";

        public async Task<VehicleType> AddUpdateVehicleTypeAsync(string type)
        {
            var item = await VehicleTypes.FirstOrDefaultAsync(i => i.Type == type);
            if (item != null) return item;
            item = new VehicleType {Type = type};
            await VehicleTypes.AddAsync(item);
            await SaveChangesAsync();
            _cache.Invalidate(TYPES_CACHE_KEY);
            return item;
        }

        public async Task<List<VehicleType>> GetTypes()
        {
            return await Task.Run(() => { return _cache.Use(TYPES_CACHE_KEY, () => VehicleTypes.ToList()); });
        }
    }
}
