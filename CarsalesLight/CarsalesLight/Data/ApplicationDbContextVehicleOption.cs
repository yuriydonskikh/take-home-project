﻿using System.Threading.Tasks;
using CarsalesLight.Models.Entities;
using CarsalesLight.Models.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace CarsalesLight.Data
{
    public partial class ApplicationDbContext
    {
        public async Task<VehicleOption> AddUpdateVehicleOptionAsync(OptionViewModel option, int vehicleId)
        {
            var opt = await VehicleOptions.FirstOrDefaultAsync(i => i.Option == option.Option && i.VehicleId == vehicleId);
            if (opt == null)
            {
                opt = new VehicleOption
                {
                    Option = option.Option,
                    VehicleId = vehicleId
                };
                await VehicleOptions.AddAsync(opt);
                await SaveChangesAsync();
            }
            opt.Comment = option.Comment;
            opt.IsDefault = option.IsDefault;
            Update(opt);
            await SaveChangesAsync();
            return opt;
        }

        public async Task DeleteOptionAsync(int optionId)
        {
            var option = await VehicleOptions.FirstOrDefaultAsync(i => i.Id == optionId);
            VehicleOptions.Remove(option);
            await SaveChangesAsync();
        }

    }
}
