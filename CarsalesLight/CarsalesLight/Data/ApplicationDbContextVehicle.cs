﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CarsalesLight.Helpers;
using CarsalesLight.Models.Entities;
using CarsalesLight.Models.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace CarsalesLight.Data
{
    public partial class ApplicationDbContext
    {
        public async Task<Vehicle> AddVehicleAsync(VehicleViewModel vehicle, string userId)
        {
            var customer = await GetCustomerAsync(userId);
            var model = await AddMakeModelAsync(vehicle.MakeModel);
            var type = await AddUpdateVehicleTypeAsync(vehicle.Type);
            var result = new Vehicle
            {
                ModelId = model.Id,
                Comments = vehicle.Comments,
                CustomerId = customer.Id,
                IsDriveAwayPrice = vehicle.IsDriveAwayPrice,
                Price = vehicle.Price,
                Year = vehicle.Year,
                TypeId = type.Id,
                Options = new List<VehicleOption>()
            };
            foreach (var optionDto in vehicle.Options)
            {
                var option = _mapper.Map<VehicleOption>(optionDto);
                result.Options.Add(option);
            }
            Vehicles.Add(result);
            await SaveChangesAsync();
            return result;
        }

        public async Task<Vehicle> EditVehicleAsync(int id, VehicleViewModel model)
        {
            var vmodel = await AddMakeModelAsync(model.MakeModel);
            var type = await AddUpdateVehicleTypeAsync(model.Type);
            var result = await Vehicles.Include(i=>i.Options).FirstOrDefaultAsync(i => i.Id == id);
            result.ModelId = vmodel.Id;
            result.Comments = model.Comments;
            result.IsDriveAwayPrice = model.IsDriveAwayPrice;
            result.Price = model.Price;
            result.Year = model.Year;
            result.TypeId = type.Id;
            result.Options.Clear();
            foreach (var optionDto in model.Options)
            {
                var option = _mapper.Map<VehicleOption>(optionDto);
                result.Options.Add(option);
            }
            Update(result);
            await SaveChangesAsync();
            return result;
        }

        public async Task DeleteVehicleAsync(int id)
        {
            var vehicle = await Vehicles.FirstOrDefaultAsync(i => i.Id == id);
            Vehicles.Remove(vehicle);
            await SaveChangesAsync();
        }

        public async Task<Vehicle> GetVehicleAsync(int id)
        {
            var vehicle = await Vehicles.Include(i=>i.Options).FirstOrDefaultAsync(i => i.Id == id);
            return vehicle;
        }

        public async Task<List<VehicleViewModel>> GetVehiclesAsync(string userId)
        {
            var vehicles = await Vehicles.Include(i=>i.Options).Include(i=>i.Model.Make).Include(i=>i.Customer.Identity).Where(i=>i.Customer.Identity.Id == userId).ToListAsync();
            var result = _mapper.Map<List<VehicleViewModel>>(vehicles);
            return result;
        }

        public async Task<List<VehicleViewModel>> GetVehiclesAsync(VehicleQueryViewModel query)
        {
            var vehicles = (await Vehicles.Include(i => i.Options).Include(i => i.Model.Make).Include(i => i.Customer.Identity).ToListAsync()).Where(i => i.IsQueryable(query));
            var result = _mapper.Map<List<VehicleViewModel>>(vehicles);
            return result;
        }
    }
}
