﻿using AutoMapper;
using CarsalesLight.Models.Entities;
using CarsalesLight.Services;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace CarsalesLight.Data
{
    public partial class ApplicationDbContext : IdentityDbContext<AppUser>
    {
        private readonly IMapper _mapper;
        private readonly ICacheController _cache;

        public ApplicationDbContext(DbContextOptions options, IMapper mapper, ICacheController cache)
            : base(options)
        {
            _mapper = mapper;
            _cache = cache;
        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<VehicleMake> VehicleMakes { get; set; }
        public DbSet<VehicleModel> VehicleModels { get; set; }
        public DbSet<VehicleOption> VehicleOptions { get; set; }
        public DbSet<VehicleType> VehicleTypes { get; set; }
    }

}
