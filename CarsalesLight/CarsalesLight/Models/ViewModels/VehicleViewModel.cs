﻿using System.Collections.Generic;

namespace CarsalesLight.Models.ViewModels
{
    public class VehicleViewModel
    {
        public MakeModelViewModel MakeModel { get; set; }
        public string Type { get; set; }
        public int Year { get; set; }
        public List<OptionViewModel> Options { get; set; }
        public string Comments { get; set; }
        public decimal Price { get; set; }
        public bool IsDriveAwayPrice { get; set; }
        public PrivateCustomerViewModel Private { get; set; }
        public DealerViewModel Dealer { get; set; }
    }
}
