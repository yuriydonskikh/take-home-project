﻿using AutoMapper;
using CarsalesLight.Models.Entities;

namespace CarsalesLight.Models.ViewModels
{
    public class ViewModelMapper:Profile
    {
        public ViewModelMapper()
        {
            CreateMap<CustomerRegistrationViewModel, AppUser>()
                .ForMember(dst => dst.UserName, src => src.MapFrom(vm => vm.Email));

            CreateMap<Vehicle, VehicleViewModel>()
                .ForMember(dst => dst.Dealer,
                    opt => opt.MapFrom(
                        src => src.Customer.Identity.IsDealer ? new DealerViewModel
                        {
                            ABN = src.Customer.Identity.ABN,
                            Email = src.Customer.Identity.Email
                        } : null))
                .ForMember(dst => dst.Private,
                    opt => opt.MapFrom(
                        src=> !src.Customer.Identity.IsDealer ? new PrivateCustomerViewModel
                        {
                            Email = src.Customer.Identity.Email,
                            FirstName = src.Customer.Identity.FirstName,
                            LastName = src.Customer.Identity.LastName,
                            PhoneNumber = src.Customer.Identity.PhoneNumber
                        } : null))
                .ForMember(dst => dst.MakeModel, opt => opt.MapFrom(src => src.Model));

            CreateMap<VehicleOption, OptionViewModel>().ReverseMap();

            CreateMap<Vehicle, VehicleQueryViewModel>()
                .ForMember(dst => dst.Model, opt => opt.MapFrom(src => src.Model.Model))
                .ForMember(dst => dst.Make, opt => opt.MapFrom(src => src.Model.Make.Make))
                .ForMember(dst => dst.Type, opt => opt.MapFrom(src => src.Type.Type))
                .ForMember(dst => dst.IncludeDealer, opt => opt.MapFrom(src => src.Customer.Identity.IsDealer))
                .ForMember(dst => dst.IncludePrivate, opt => opt.MapFrom(src => !src.Customer.Identity.IsDealer))
                .ForMember(dst => dst.PriceFrom, opt => opt.MapFrom(src => src.Price))
                .ForMember(dst => dst.PriceTo, opt => opt.MapFrom(src => src.Price))
                .ForMember(dst => dst.YearFrom, opt => opt.MapFrom(src => src.Year))
                .ForMember(dst => dst.YearTo, opt => opt.MapFrom(src => src.Year));

            CreateMap<VehicleType, InfoViewModel>()
                .ForMember(dst => dst.Info, opt => opt.MapFrom(src => src.Type)).ReverseMap();

            CreateMap<VehicleMake, InfoViewModel>()
                .ForMember(dst => dst.Info, opt => opt.MapFrom(src => src.Make)).ReverseMap();

            CreateMap<VehicleModel, MakeModelViewModel>()
                .ForMember(dst => dst.Make, opt => opt.MapFrom(src => src.Make.Make))
                .ForMember(dst => dst.Model, opt => opt.MapFrom(src => src.Model));

            CreateMap<MakeModelViewModel, VehicleModel>()
                .ForMember(dst => dst.Make, opt => opt.MapFrom(src => new VehicleMake{Make = src.Make}))
                .ForMember(dst => dst.Model, opt => opt.MapFrom(src => src.Model));
        }
    }
}
