﻿namespace CarsalesLight.Models.ViewModels
{
    public class VehicleQueryViewModel
    {
        public string Make { get; set; }
        public string Model { get; set; }
        public string Type { get; set; }
        public int YearFrom { get; set; }
        public int YearTo { get; set; }
        public decimal PriceFrom { get; set; }
        public decimal PriceTo { get; set; }
        public bool IncludePrivate { get; set; }
        public bool IncludeDealer { get; set; }
        public string Filter { get; set; }
    }
}
