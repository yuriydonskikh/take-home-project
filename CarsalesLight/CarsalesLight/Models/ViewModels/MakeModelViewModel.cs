﻿namespace CarsalesLight.Models.ViewModels
{
    public class MakeModelViewModel
    {
        public string Make { get; set; }
        public string Model { get; set; }
    }
}
