﻿namespace CarsalesLight.Models.ViewModels
{
    public class DealerViewModel
    {
        public string ABN { get; set; }
        public string Email { get; set; }
    }
}