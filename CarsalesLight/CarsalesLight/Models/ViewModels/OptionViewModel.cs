﻿namespace CarsalesLight.Models.ViewModels
{
    public class OptionViewModel
    {
        public string Option { get; set; }
        public string Comment { get; set; }
        public bool IsDefault { get; set; }
    }
}
