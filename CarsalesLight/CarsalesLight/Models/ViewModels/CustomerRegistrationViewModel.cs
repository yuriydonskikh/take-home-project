﻿namespace CarsalesLight.Models.ViewModels
{
    public class CustomerRegistrationViewModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Company { get; set; }
        public string ABN { get; set; }
        public bool IsDealer { get; set; }
    }
}
