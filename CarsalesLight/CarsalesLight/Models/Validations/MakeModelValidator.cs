﻿using CarsalesLight.Models.ViewModels;
using FluentValidation;

namespace CarsalesLight.Models.Validations
{
    public class MakeModelValidator : AbstractValidator<MakeModelViewModel>
    {
        public MakeModelValidator()
        {
            RuleFor(vm => vm.Make).NotEmpty().WithMessage("Make cannot be empty");
            RuleFor(vm => vm.Model).NotEmpty().WithMessage("Make cannot be empty");
        }
    }
}
