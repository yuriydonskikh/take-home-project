﻿using CarsalesLight.Models.Entities;
using FluentValidation;

namespace CarsalesLight.Models.Validations
{
    public class VehicleTypeValidator:AbstractValidator<VehicleType>
    {
        public VehicleTypeValidator()
        {
            RuleFor(vm => vm.Type).NotEmpty().WithMessage("VehicleType cannot be empty");
        }
    }
}
