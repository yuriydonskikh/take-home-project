﻿using System;
using CarsalesLight.Models.ViewModels;
using FluentValidation;

namespace CarsalesLight.Models.Validations
{
    public class VehicleQueryValidator : AbstractValidator<VehicleQueryViewModel>
    {
        public VehicleQueryValidator()
        {
            RuleFor(vm => vm.Model).NotEmpty().When(vm=>string.IsNullOrEmpty(vm.Make)&&string.IsNullOrEmpty(vm.Type)).WithMessage("At least one of [Make, Model, Type, Filter] must be not empty");
            RuleFor(vm => vm.Make).NotEmpty().When(vm => string.IsNullOrEmpty(vm.Model) && string.IsNullOrEmpty(vm.Type)).WithMessage("At least one of [Make, Model, Type, Filter] must be not empty");
            RuleFor(vm => vm.Type).NotEmpty().When(vm => string.IsNullOrEmpty(vm.Make) && string.IsNullOrEmpty(vm.Model)).WithMessage("At least one of [Make, Model, Type, Filter] must be not empty");
            RuleFor(vm => vm.IncludeDealer).Equal(true).When(vm=> !vm.IncludePrivate).WithMessage("Must include dealers' or private vehicles or both");
            RuleFor(vm => vm.IncludePrivate).Equal(true).When(vm => !vm.IncludeDealer).WithMessage("Must include dealers' or private vehicles or both");
            RuleFor(vm => vm.YearFrom).Must(value => value == 0 || value >= DateTime.Now.AddYears(-150).Year && value <= DateTime.Now.AddYears(1).Year).WithMessage($"YearFrom must be between {DateTime.Now.AddYears(-150).Year} and {DateTime.Now.AddYears(1).Year}");
            RuleFor(vm => vm.YearTo).Must(value => value == 0 || value >= DateTime.Now.AddYears(-150).Year && value <= DateTime.Now.AddYears(1).Year).WithMessage($"YearTo must be between {DateTime.Now.AddYears(-150).Year} and {DateTime.Now.AddYears(1).Year}");
        }
    }
}