﻿using CarsalesLight.Models.ViewModels;
using FluentValidation;

namespace CarsalesLight.Models.Validations
{
    public class VehicleOptionValidator : AbstractValidator<OptionViewModel>
    {
        public VehicleOptionValidator()
        {
            RuleFor(vm => vm.Option).NotEmpty().WithMessage("Option cannot be empty");
        }
    }
}
