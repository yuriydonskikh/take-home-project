﻿using System;
using CarsalesLight.Models.ViewModels;
using FluentValidation;

namespace CarsalesLight.Models.Validations
{
    public class VehicleValidator : AbstractValidator<VehicleViewModel>
    {
        public VehicleValidator()
        {
            RuleFor(vm => vm.MakeModel).SetValidator(new MakeModelValidator());
            RuleFor(vm => vm.Type).NotEmpty().WithMessage("Type cannot be empty");
            RuleFor(vm => vm.Year).InclusiveBetween(DateTime.Now.AddYears(-150).Year, DateTime.Now.AddYears(1).Year)
                .WithMessage(
                    $"Year must be between {DateTime.Now.AddYears(-150).Year} and {DateTime.Now.AddYears(1).Year}");
            RuleForEach(vm => vm.Options).SetValidator(new VehicleOptionValidator());
            RuleFor(vm => vm.Price).GreaterThanOrEqualTo(0).WithMessage("The price cannot be less then zero");
        }
    }
}