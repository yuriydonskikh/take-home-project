﻿namespace CarsalesLight.Models.Entities
{
    public class VehicleModel
    {
        public virtual int Id { get; set; }
        public virtual int MakeId { get; set; }
        public virtual VehicleMake Make { get; set; }
        public virtual string Model { get; set; }
    }
}