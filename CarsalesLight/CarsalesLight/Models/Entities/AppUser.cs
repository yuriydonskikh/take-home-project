﻿using Microsoft.AspNetCore.Identity;

namespace CarsalesLight.Models.Entities
{
    // Add profile data for application users by adding properties to this class
    public class AppUser : IdentityUser
    {
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string Company { get; set; }
        public virtual string ABN { get; set; }
        public virtual bool IsDealer { get; set; }
    }
}
