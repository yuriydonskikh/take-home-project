﻿using System.Collections.Generic;

namespace CarsalesLight.Models.Entities
{
    public class Vehicle
    {
        public virtual int Id { get; set; }
        public virtual int ModelId { get; set; }
        public virtual VehicleModel Model { get; set; }
        public virtual int TypeId { get; set; }
        public virtual VehicleType Type { get; set; }
        public virtual int Year { get; set; }
        public virtual ICollection<VehicleOption> Options { get; set; }
        public virtual string Comments { get; set; }
        public virtual int CustomerId { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual decimal Price { get; set; }
        public virtual bool IsDriveAwayPrice { get; set; }
    }
}