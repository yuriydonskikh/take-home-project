﻿namespace CarsalesLight.Models.Entities
{
    public class VehicleType
    {
        public virtual int Id { get; set; }
        public virtual string Type { get; set; }
    }
}