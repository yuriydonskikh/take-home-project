﻿namespace CarsalesLight.Models.Entities
{
    public class VehicleMake
    {
        public virtual int Id { get; set; }
        public virtual string Make { get; set; }
    }
}
