﻿namespace CarsalesLight.Models.Entities
{
    public class VehicleOption
    {
        public virtual int Id { get; set; }
        public virtual string Option { get; set; }
        public virtual string Comment { get; set; }
        public virtual bool IsDefault { get; set; }
        public virtual int VehicleId { get; set; }
    }
}