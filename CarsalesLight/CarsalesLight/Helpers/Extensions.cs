﻿using System;
using CarsalesLight.Models.Entities;
using CarsalesLight.Models.ViewModels;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace CarsalesLight.Helpers
{
    public static class Extensions
    {
        public static void AddApplicationError(this HttpResponse response, string message)
        {
            response.Headers.Add("Application-Error", message);
            response.Headers.Add("access-control-expose-headers", "Application-Error");
        }

        public static bool BetweenOrDefault(this int val, int min, int max, int minval)
        {
            return BetweenOrDefault(val, min, max, (decimal) minval);
        }

        public static bool BetweenOrDefault(this decimal val, decimal min, decimal max, decimal minval)
        {
            if (min == minval && max == minval) return true;
            if (min > minval && max > minval && val >= min && val <= max) return true;
            if (max == 0 && min > minval && val >= min) return true;
            if (min == 0 && max > minval && val <= max) return true;
            return false;
        }

        public static bool ContainsText<T>(this T value, string searchtext)
        {
            var text = JsonConvert.SerializeObject(value);
            return text.Contains(searchtext, StringComparison.InvariantCultureIgnoreCase);
        }

        public static bool IsQueryable(this Vehicle vehicle, VehicleQueryViewModel query)
        {
            if (!string.IsNullOrEmpty(query.Make) && !string.Equals(query.Make, vehicle.Model?.Make?.Make, StringComparison.InvariantCultureIgnoreCase)) return false;
            if (!string.IsNullOrEmpty(query.Model) && !string.Equals(query.Model, vehicle.Model?.Model, StringComparison.InvariantCultureIgnoreCase)) return false;
            if (!string.IsNullOrEmpty(query.Type) && !string.Equals(query.Type, vehicle.Type?.Type, StringComparison.InvariantCultureIgnoreCase)) return false;
            if (!vehicle.Year.BetweenOrDefault(query.YearFrom, query.YearTo, 0)) return false;
            if (!vehicle.Price.BetweenOrDefault(query.PriceFrom, query.PriceTo, 0)) return false;
            if (!query.IncludeDealer && vehicle.Customer.Identity.IsDealer) return false;
            if (!query.IncludePrivate && !vehicle.Customer.Identity.IsDealer) return false;
            if (!string.IsNullOrEmpty(query.Filter) && !vehicle.ContainsText(query.Filter)) return false;
            return true;
        }

        public static long ToUnixDate(this DateTime date)
        {
            return (long)date.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
        }
    }
}
