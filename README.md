# CarsalesLight WEB Api
## take-home-project

### Prerequisites
  
  a. Visual Studio 2017 any edition,  version 15.7 or above
  
  b. .Net Core SDK 2.1
  
### Build & Run
  
  a. Open solution in Visual Studio
  
  b. Install all requested updates (happened if Visual Studio is not up to date, or missed important components)
  
  c. Choose **Build Solution** from **Build** menu
  
  d. Click **Start Debugging** from **Debug** menu
  
  *Above steps have to download all nuget packages rebuild project and start default browser with* **SwaggerUI**
  
### Used non default components and fremeworks
  
  a. AutoMapper - *mapping tool for converting classes between Entities and DTOs*
  
  b. NLog - *logging errors*
  
  c. FluentValidation - *validate data before executing controller*
  
  d. Sqlite - *Simple database for data persistent*
  
  e. Swashbuckle Swagger - *WEB Api documentation and testing*
  
  f. xUnit - *testing framework*
  
  g. AutoFixture - *tool for creating prepopulated objects for testing*
  
  h. FluentAssertions - *tool for conditional testing*
  
  i. Moq - *Mocking tool for mocking object behaveour during testing*
  
  *All above pachages will be automatically downloaded during first build*
  
### Unit Tests
  
  Solution contains **36** tests which are covered all sensitive areas of projects
  
  a. AccountControllerTests (1) - *contains unit test for account controller*
  
  b. AuthControllerTests (1) - *contains unit test for auth controller*
  
  c. RetrieveVehiclesControllerTests (5) - *contains unit tests for retrieve vehicles controller*
  
  d. VehicleControllerTests (4) - *contains unit tests for vehicle controller*
  
  e. VehicleInfoControllerTests (7) - *contains unit tests for vehicle info controller*
  
  f. ExtensionsTests (4) - *contains unit tests all data monipulation extensions*
  
  g. MappingTests (8) - *contains unit tests for all mappers*
  
  h. ValidatorsTests (6) - *contains unit tests for all validators*
  
### Run Tests

  a. Run from Visual Studio - Choose **Test->Run->All Tests**
  
  b. Run from command prompt - from CarsalesLight.Tests folder execute command **dotnet test**

### Migrate to another database

  Solution contains migration script for creating database from POCO entities inside the solution also it uses for emulate database during tests.

  
  For switching to another database please change connection string in appsettings.json file and perfom migration:
  
  
  a. From Visual Studio open Package Manager Console and execute **update-database**
  
  b. From command prompt inside CarsalesLight project folder execute command **dotnet ef database update**  
  
  *Due to differebces in the structure of databases script can fail, for rectifying it - need to fix stractures inside files in Migrations folder*
  
### Postman scripts

  Please note I also include full lifecicle of requests in Postman format you can find folder Postman inside solution folder.
  
  Stored request allow simply check every request available in the solution including requests required authentification.
  
  The Postman with documentation can be downloade from here: https://www.getpostman.com/
  